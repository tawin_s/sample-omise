require 'rails_helper'

RSpec.describe "transactions/edit", type: :view do
  before(:each) do
    @transaction = assign(:transaction, Transaction.create!(
      :amonut => 1.5,
      :user_id => 1
    ))
  end

  it "renders the edit transaction form" do
    render

    assert_select "form[action=?][method=?]", transaction_path(@transaction), "post" do

      assert_select "input#transaction_amonut[name=?]", "transaction[amonut]"

      assert_select "input#transaction_user_id[name=?]", "transaction[user_id]"
    end
  end
end
