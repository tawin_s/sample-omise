require 'rails_helper'

RSpec.describe "transactions/new", type: :view do
  before(:each) do
    assign(:transaction, Transaction.new(
      :amonut => 1.5,
      :user_id => 1
    ))
  end

  it "renders new transaction form" do
    render

    assert_select "form[action=?][method=?]", transactions_path, "post" do

      assert_select "input#transaction_amonut[name=?]", "transaction[amonut]"

      assert_select "input#transaction_user_id[name=?]", "transaction[user_id]"
    end
  end
end
