module SampleOmise
  class OmiseApi
    module Charge
      # Create new charge
      def create_charge(options={})
        post('/charges', options)
      end
    end
  end
end

