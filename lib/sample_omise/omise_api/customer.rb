module SampleOmise
  class OmiseApi
    module Customer

      # Get customer list from omise
      def get_customers(options= {})
        get('/customers', options)
      end

      # Get customer detail
      def get_customer(omise_customer_id, options={})
        get("/customers/#{omise_customer_id}", options)
      end

      # Create new customer at omise
      def create_customer(options = {})
        post('/customers', options)
      end

      # Update customer at omise. Require customer id.
      def update_customer(omise_customer_id, options = {})
        patch("/customers/#{omise_customer_id}", options)
      end

      def delete_customer(omise_customer_id, options ={})
        delete("/customers/#{omise_customer_id}", options)
      end
    end
  end
end