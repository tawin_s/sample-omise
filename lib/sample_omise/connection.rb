require 'faraday_middleware'
Dir[File.expand_path('../faraday/*.rb', __FILE__)].each { |f| require f }
module SampleOmise
  module Connection
    private

    def connection
      options = {
        headers: {'Accept' => "application/#{format_header};",  'User-Agent' => user_agent},
        url: omise_end_point_url
      }

      Faraday::Connection.new(options) do |connection|
        connection.use Faraday::Request::BasicAuthentication, omise_secret_key, ''
        connection.use Faraday::Response::Logger
        connection.use Faraday::Response::ParseJson
        connection.use Faraday::Request::UrlEncoded
        connection.use FaradayMiddleware::Mashify
        connection.use FaradayMiddleware::RaiseHttpException
        connection.adapter adapter
      end
    end
  end
end