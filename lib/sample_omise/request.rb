module SampleOmise
  module Request
    def get(path, options={})
      request(:get, path, options)
    end

    def post(path, options={})
      request(:post, path, options)
    end

    def put(path, options={})
      request(:put, path, options)
    end

    def patch(path, options={})
      request(:patch, path, options)
    end

    def delete(path, options={})
      request(:delete, path, options)
    end


    private

    def request(method, path, options)
      response = connection.send(method) do |request|
        case method
          when :get, :delete
            request.url(path, options)
          when :post, :put, :patch
            request.path = path
            request.body = options unless options.empty?
        end
      end
      if response.body && response.body['data']
        return response.body['data']
      elsif response.body
        return response.body
      else
        return response
      end

    end
  end
end