require 'faraday'
require 'typhoeus'
require 'typhoeus/adapters/faraday'
require File.expand_path('../version', __FILE__)

module SampleOmise
  # Defines constants and methods related to configuration
  module Configuration
    # An array of valid keys in the options hash
    VALID_OPTIONS_KEYS = [
      :adapter,
      :format_header,
      :user_agent,
      :proxy,
      :omise_secret_key,
      :omise_public_key,
      :omise_end_point_url
    ].freeze

    # An array of valid request/response formats
    #
    # @note Not all methods support the XML format.
    VALID_FORMATS = [
      :json].freeze

    # By defulat set adapter to typhoeus
    DEFAULT_ADAPTER = :typhoeus

    # By default don't set omise secret key
    DEFAULT_OMISE_SECRET_KEY = nil

    # By default don't set omise public key
    DEFAULT_OMISE_PUBLIC_KEY = nil

    # By default set omise endpoint url to
    DEFAULT_OMISE_END_POINT_URL = 'https://api.omise.co'.freeze

    # By default use json format
    DEFAULT_FORMAT = :json

    # Set up user agent
    DEFAULT_USER_AGENT = "Sample Omise Gem #{SampleOmise::VERSION}".freeze

    #Set up proxy
    DEFAULT_PROXY = nil

    # @private
    attr_accessor *VALID_OPTIONS_KEYS

    # When this module is extended, set all configuration options to their default values
    def self.extended(base)
      base.reset
    end

    # Convenience method to allow configuration options to be set in a block
    def configure
      yield self
    end

    # Create a hash of options and their values
    def options
      VALID_OPTIONS_KEYS.inject({}) do |option, key|
        option.merge!(key => send(key))
      end
    end

    # Reset all configuration options to defaults
    def reset
      self.omise_secret_key = DEFAULT_OMISE_SECRET_KEY
      self.omise_public_key = DEFAULT_OMISE_PUBLIC_KEY
      self.omise_end_point_url = DEFAULT_OMISE_END_POINT_URL
      self.adapter = DEFAULT_ADAPTER
      self.format_header = DEFAULT_FORMAT
      self.proxy = DEFAULT_PROXY
      self.user_agent = DEFAULT_USER_AGENT
    end


  end
end