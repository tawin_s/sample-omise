module SampleOmise
  class OmiseApi < Api
  Dir[File.expand_path('../omise_api/*.rb', __FILE__)].each{|f| require f}

  include SampleOmise::OmiseApi::Customer
  include SampleOmise::OmiseApi::Charge

  end
end