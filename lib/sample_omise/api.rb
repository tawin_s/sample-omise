require File.expand_path('../connection', __FILE__)
require File.expand_path('../request', __FILE__)

module SampleOmise
  class Api
      include Connection
    include Request

    attr_accessor *Configuration::VALID_OPTIONS_KEYS

    def initialize(options={})
      options = SampleOmise.options.merge(options)
      Configuration::VALID_OPTIONS_KEYS.each do |key|
        send("#{key}=", options[key])
      end
    end

  end
end