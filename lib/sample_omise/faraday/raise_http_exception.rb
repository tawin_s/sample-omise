require 'faraday'


# TODO: Structure to handle the json response of the errors
# @private
module FaradayMiddleware
  # @private
  class RaiseHttpException < Faraday::Middleware
    def call(env)
      @app.call(env).on_complete do |response|
        case response[:status].to_i
        when 400
          raise SampleOmise::BadRequest, error_message_400(response, "Bad request.")
        when 401
          raise SampleOmise::Unauthorized, error_message_401(response)
        when 402
          raise SampleOmise::InsufficientBalance, error_message_401(response)
        when 404
          raise SampleOmise::NotFound, error_message_400(response, "Not found.")
        when 500
          raise SampleOmise::InternalServerError, error_message_500(response, "Something is technically wrong.")
        when 503
          raise SampleOmise::ServiceUnavailable, error_message_500(response, "Omise is rate limiting your requests.")
        end
      end
    end

    def initialize(app)
      super app
      @parser = nil
    end

    private

    def error_message_400(response, body=nil)
      JSON.parse(response.body)["error_description"]
    end

    def error_message_401(response)
      unless response.body.blank?
        error_response = JSON.parse(response.body)
        error_response["error_description"] if error_response["error"]
      end
    end

    def error_message_500(response, body=nil)
      "#{response[:method].to_s.upcase} #{response[:url].to_s}: #{[response[:status].to_s + ':', body].compact.join(' ')}"
    end
  end
end
