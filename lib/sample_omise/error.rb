module SampleOmise
  # Custom error class for rescuing from all SampleOmise errors
  class Error < StandardError; end

  # Raised when SampleOmise returns the HTTP status code 400
  class BadRequest < Error; end

  # Raised when SampleOmise returns the HTTP status code 401
  class Unauthorized < Error; end

  # Raised when SampleOmise returns the HTTP status code 401 and it's because the token is expired
  class ExpiredAccessToken < Error; end

  # Raised when SampleOmise returns the HTTP status code 404
  class NotFound < Error; end

  # Raised when SampleOmise returns the HTTP status code 422
  class UnprocessableEntity < Error; end

  # Raised when SampleOmise returns the HTTP status code 500
  class InternalServerError < Error; end

  # Raised when SampleOmise returns the HTTP status code 503
  class ServiceUnavailable < Error; end
end
