require File.expand_path('../sample_omise/error', __FILE__)
require File.expand_path('../sample_omise/configuration', __FILE__)
require File.expand_path('../sample_omise/api', __FILE__)
require File.expand_path('../sample_omise/omise_api', __FILE__)

module SampleOmise
  extend Configuration

  def self.omise_api(options={})
    SampleOmise::OmiseApi.new(options)
  end
end