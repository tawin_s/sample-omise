require 'sample_omise'
fail("Don't forget to set up omise key. You can remove this line after set up the keys.") unless ENV['OMISE_SECRET_KEY'] && ENV['OMISE_PUBLIC_KEY']

SampleOmise.configure do |config|
  # Set your omise secret key
  config.omise_secret_key = ENV['OMISE_SECRET_KEY']

  # Set your omise public key
  config.omise_public_key = ENV['OMISE_PUBLIC_KEY']

  # These are the valid options that you can set up.
  # Adapter for faraday gem to make a request by default
  # config.adapter = :typhoeus
  # Set format in the request header by default is json
  # config.format_header = :json
  # Set the proxy header for faraday gem
  # config.proxy
  # Set the endpoint url to call omise
  # config.omise_end_point_url
end