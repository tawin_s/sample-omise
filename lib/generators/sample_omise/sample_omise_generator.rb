class SampleOmiseGenerator < Rails::Generators::Base
  source_root File.expand_path('../templates', __FILE__)

  def create_initializer_file
    copy_file "sample_omise.rb", "config/initializers/sample_omise.rb"
  end
end
