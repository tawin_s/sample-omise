class AddTransactionCountToCustomer < ActiveRecord::Migration
  def up
    add_column :customers, :transactions_count, :integer, :default => 0
    Customer.all.each do |customer|
      Customer.update_counters customer.id, transactions_count: customer.transactions.length
    end
  end

  def down
    remove_column :customers, :transactions_count
  end
end
