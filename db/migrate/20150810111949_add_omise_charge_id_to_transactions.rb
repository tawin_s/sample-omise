class AddOmiseChargeIdToTransactions < ActiveRecord::Migration
  def change
    add_column :transactions, :omise_charge_id, :string
  end
end
