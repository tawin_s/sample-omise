require 'sample_omise'
fail("Don't forget to set up omise key. You can remove this line after set up the keys.") unless ENV['OMISE_SECRET_KEY'] && ENV['OMISE_PUBLIC_KEY']

SampleOmise.configure do |config|
  config.omise_secret_key = ENV['OMISE_SECRET_KEY']
  config.omise_public_key = ENV['OMISE_PUBLIC_KEY']
end