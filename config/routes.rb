Rails.application.routes.draw do
  use_doorkeeper
  root 'welcome#index'
  get 'dashboard/index'

  resources :transactions, only: [:show, :index, :create, :new]
  resources :customers do
    resources :transactions, only: [:show, :index, :create, :new]
    member do
      patch 'update_omise_cards'
    end
  end

  constraints subdomaion: 'api'do
    scope module: 'api', defaults: {format: :json} do
      namespace :v1 do
        resources :customers
      end
    end
  end


  devise_for :user

end
