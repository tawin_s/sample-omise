class WelcomeController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index]

  layout 'full_screen'
  def index

  end
end
