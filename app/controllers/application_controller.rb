class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!
  layout :layout_for_devise

  # Set up devise after log in path to transactions page
  def after_sign_in_path_for user
    dashboard_index_path
  end


  def layout_for_devise
    if devise_controller?
      "full_screen"
    end
  end
end
