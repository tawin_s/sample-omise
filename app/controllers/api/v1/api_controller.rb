class Api::V1::ApiController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  before_action :doorkeeper_authorize!

  protected

  def current_user
    @current_user ||= User.where(id: doorkeeper_token.resource_owner_id).first if doorkeeper_token
  end

  def authenticate!
    if current_user
      head :unauthorized
    end
    head :unauthorized unless current_user
  end

end