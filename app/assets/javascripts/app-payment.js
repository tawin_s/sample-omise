(function (window) {
  'use strict';
  if (!window.SampleOmiseApp) window.SampleOmiseApp = {};

  var SampleOmiseApp = window.SampleOmiseApp;

  SampleOmiseApp.Payment = {
    SetupOmise: function(){
      window.Omise.setPublicKey('pkey_test_50xk1rz1lysh9ivoc75');
    },

    Init: function(){
      this.SetupOmise();
    }
  }
  }(window));