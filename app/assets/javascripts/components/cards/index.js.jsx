window.ListCardsComponent = React.createClass({
  getInitialState: function () {
    return {
      initialCards: this.props.cards,
      cards: []
    };
  },
  componentWillMount: function () {
    this.setState({cards: this.state.initialCards});
  },
  addNewCardToTable(card){
    var cards = this.state.cards.slice()
    cards.push(card)
    this.setState({cards: cards})
  },
  render: function () {
    return (
      <div>
        {/* Hello world */}
        <table className='table'>
          <tr>
            <th>Last 4 Digits</th>
            <th>Brand</th>
            <th>Financing</th>
          </tr>
          <ListCardRow cards={this.state.cards}/>
        </table>
        <NewCard handelNewCard={this.addNewCardToTable} customerId={this.props.customer_id}/>
      </div>
    );
  }
});

var ListCardRow = React.createClass({
  render: function () {
    return (
      <tbody>
      {
        this.props.cards.map(function (card) {
          return (
            <tr>
              <td>**** **** **** {card.last_digits}</td>
              <td>{card.brand}</td>
              <td>{card.financing}</td>
            </tr>
          );
        })
      }
      </tbody>
    );
  }
});

var NewCard = React.createClass({
  submitNewCard: function (event) {
    event.preventDefault();
    var card, cardInfo, self;
    self = this;
    cardInfo = {
      "name": this.refs.holder_name.getDOMNode().value,
      "number": this.refs.card_number.getDOMNode().value,
      "expiration_month": this.refs.expiration_month.getDOMNode().value,
      "expiration_year": this.refs.expiration_year.getDOMNode().value,
      "security_code": this.refs.security_code.getDOMNode().value
    }

    Omise.createToken("card", cardInfo, function (statusCode, response) {
      if (statusCode == 200) {
        // Success: send back the TOKEN_ID to your server. The TOKEN_ID can be
        // found in `response.id`.
        $.ajax({
          type: 'PATCH',
          data: {'customer[card]': response.id},
          url: '/customers/' + self.props.customerId + '/update_omise_cards.json',
          success: function (response, textStatus, jqXhr) {
            card = {last_digits: response.last_digits, brand: response.brand, financing: response.financing};
            self.props.handelNewCard(card);

          },
          error: function (jqXHR, textStatus, errorThrown) {
            // log the error to the console
            console.log("The following error occured: " + textStatus, errorThrown);
          }
        });
      } else {
        // Error: display an error message. Note that `response.message` contains
        // a preformatted error message. Also note that `response.code` will be
        // "invalid_card" in case of validation error on the card.
        alert(response.code)
      }
    });
  },
  render: function () {
    return (
      <div className='well'>
        <h3>Add a new credit card</h3>

        <p className='text-info'> I don't store your card information in our database for security issue. Keep it with
          omise</p>

        <form className='form-horizontal' onSubmit={this.submitNewCard}>
          <div className='form-group'>
            <label className='sr-only'>Holder Name</label>
            <input type='text' className='form-control' placeholder='Holder Name' ref='holder_name'/>
          </div>
          <div className='form-group'>
            <label className='sr-only'>Number</label>
            <input type='number' className='form-control' placeholder='Number' ref='card_number'/>
          </div>
          <div className='form-group'>
            <label className='sr-only'>Expiration month</label>
            <input type='number' className='form-control' placeholder='Expiration Month' ref='expiration_month'/>
          </div>
          <div className='form-group'>
            <label className='sr-only'>Expiration year</label>
            <input type='text' className='form-control' placeholder='Expiration Year' ref='expiration_year'/>
          </div>
          <div className='form-group'>
            <label className='sr-only'>Security code(cvv)</label>
            <input type='text' className='form-control' placeholder='Security Code' ref='security_code'/>
          </div>
          <div className='form-group'>
            <input type='submit' className='btn btn-primary' value='Add Card'/>
          </div>
        </form>
      </div>
    );
  }
});