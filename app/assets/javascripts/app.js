var ready;
ready = function () {
  'use strict';
  SampleOmiseApp.Init = function() {
    SampleOmiseApp.Map.Init();
    SampleOmiseApp.Payment.Init();
  }();

};


$(document).ready(ready);
$(document).on('page:load', ready);