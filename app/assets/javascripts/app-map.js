(function (window) {
  'use strict';
  if (!window.SampleOmiseApp) window.SampleOmiseApp = {};

  var SampleOmiseApp = window.SampleOmiseApp;

  SampleOmiseApp.Map = {
    SetupContactUsMap: function () {
      var marker, map, tawinHouse, mapOptions, styles, contentString, infowindow;


      tawinHouse = new google.maps.LatLng(13.748249, 100.420364);
      mapOptions = {
        zoom: 16,
        center: tawinHouse,

        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: true,
        scaleControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map = new google.maps.Map(document.getElementById('contact_me_map'),
        mapOptions);

      marker = new google.maps.Marker({
        map: map,
        animation: google.maps.Animation.DROP,
        position: tawinHouse
      });
      styles = [
        {
          stylers: [
            {hue: "#a73737"},
          ]
        }, {
          featureType: "road",
          elementType: "geometry",
          stylers: [
            {lightness: 100},
            {visibility: "simplified"}
          ]
        }, {
          featureType: "road",
          elementType: "labels",
          stylers: [
            {visibility: "off"}
          ]
        }
      ];

      contentString = '<div class="marker-info-container">' +
        '<div class="marker-header">' +
        '<h5>Tawin Supayanant </h5>' +
        '</div>' +
        '<div>' +
        '<p>48/5 m Soi Ngenudom 1, Bangvak Rd,<br/>' +
        'BangcheackNang Talingchan<br/>' +
        'Bangkok 10170, Thailand<br></p>' +
        '</div>' +
        '<div class="marker-footer">' +
        '<p>Phone: +66 87 672 7814 | Email: tawin.sup@gmail.com</a> ' +
        '</div>' +
        '</div>';
      infowindow = new google.maps.InfoWindow({
        content: contentString
      });
      map.setOptions({styles: styles});
      infowindow.open(map, marker);
    },
    LoadGoogleScript: function () {
      if ($('#contact_me_map').length == 0) return;
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&' +
        'callback=SampleOmiseApp.Map.SetupContactUsMap';
      document.body.appendChild(script);
    },
    Init: function () {
      this.LoadGoogleScript();
    }
  }


}(window));
