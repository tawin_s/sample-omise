module ApplicationHelper
  def set_flash_class name
    if name == 'notice'
      'alert alert-success'
    else
      'alert alert-danger'
    end
  end

  def is_customer_tab?
    controller.class == CustomersController
  end

  def is_transaction_tab?
    controller.class == TransactionsController
  end

end
