class Customer < ActiveRecord::Base
  include OmiseApi
  has_many :transactions

  after_create :create_omise_customer
  after_update :update_omise_customer
  after_destroy :destroy_omise_customer

  validates :email, uniqueness: true

  def create_omise_customer
    omise_customer = omise_api.create_customer({description: "#{self.first_name} #{self.last_name}", email: self.email})
    self.omise_customer_id = omise_customer['id']
    self.save!
  end

  def update_omise_customer
    omise_api.update_customer(self.omise_customer_id, {description: "#{self.first_name} #{self.last_name}", email: self.email})
  end

  def destroy_omise_customer
    omise_api.delete_customer(self.omise_customer_id)
  end

end
