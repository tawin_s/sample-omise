class Transaction < ActiveRecord::Base
  include OmiseApi

  belongs_to :customer, counter_cache: true
  after_create :create_omise_charge

  def create_omise_charge()
    # Todo change return uri to ENV variable
    options = {
      currency: 'thb',
      description: "transaction id #{self.id}",
      amount: self.amonut.to_i,
      return_uri: "http://www.example.com/orders/#{self.id}/complete",
      customer: self.customer.omise_customer_id
    }
    response = omise_api.create_charge(options)
    self.omise_charge_id = response['id']
    self.save!
  end

end
